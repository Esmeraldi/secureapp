package com.aldi.secureapp.ui.main


import android.content.pm.ApplicationInfo
import android.content.pm.PackageManager
import android.content.res.Resources
import androidx.lifecycle.*
import com.aldi.secureapp.ResourceEnumerator
import com.aldi.secureapp.ResourceGroup
import java.lang.Exception

typealias PackageResourceGroup =  MutableLiveData<List<ResourceGroup>>

class PageViewModel: ViewModel() {

    var _packageManager: MutableLiveData<PackageManager> = MutableLiveData()

    var _resourceGroup: PackageResourceGroup = MutableLiveData()

    fun setPackageManager(packageManager: PackageManager) {
        _packageManager.value = packageManager
    }

    var _packageId: MutableLiveData<String> = MutableLiveData()

    fun setpackageId(packageId: String) {
        _packageId.value =  packageId
    }

    fun setResourceGroups() {
        _resourceGroup.value = getApplicationResources().take(5)
    }

    fun getApplicationInfo() : ApplicationInfo? {
        try {
            return _packageManager.value?.getApplicationInfo(_packageId.value, PackageManager.MATCH_UNINSTALLED_PACKAGES)
        } catch (e: Exception ) {
            android.util.Log.d("error", e.toString())
        }
        return null
    }

    fun getApplicationResources() : List<ResourceGroup> {
        return  ResourceEnumerator(_packageManager.value?.getResourcesForApplication(_packageId.value)).getResourceGroups()
    }

}