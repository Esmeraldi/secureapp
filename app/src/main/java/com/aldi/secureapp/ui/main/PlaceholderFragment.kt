package com.aldi.secureapp.ui.main
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import com.aldi.secureapp.R
import com.aldi.secureapp.ResourceEntry

/**
 * A placeholder fragment containing a simple view.
 */
class PlaceholderFragment(val resourceEntry: List<ResourceEntry>) : Fragment() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
    }

    override fun onCreateView(
            inflater: LayoutInflater, container: ViewGroup?,
            savedInstanceState: Bundle?
    ): View {
        val root = inflater.inflate(R.layout.fragment_main, container, false)
        return root
    }

    companion object {
        /**
         * The fragment argument representing the section number for this
         * fragment.
         */
        private const val ARG_SECTION_NUMBER = "section_number"

        /**
         * Returns a new instance of this fragment for the given section
         * number.
         */
        @JvmStatic
        fun newInstance( _resources: List<ResourceEntry>): PlaceholderFragment {
            return PlaceholderFragment(_resources)
        }
    }
}