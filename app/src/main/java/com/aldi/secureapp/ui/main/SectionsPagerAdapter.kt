package com.aldi.secureapp.ui.main

import android.content.Context
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentPagerAdapter
import com.aldi.secureapp.R
import com.aldi.secureapp.ResourceGroup
import com.aldi.secureapp.ResourceList.ResourcesListFragment

private val TAB_TITLES = arrayOf(
        R.string.tab_text_1,
        R.string.tab_text_2
)

/**
 * A [FragmentPagerAdapter] that returns a fragment corresponding to
 * one of the sections/tabs/pages.
 */
class SectionsPagerAdapter(private val context: Context, private  val fm: FragmentManager, val _vm: PageViewModel, val _resItems: List<ResourceGroup>)
    : FragmentPagerAdapter(fm) {

    override fun getItem(position: Int): Fragment {
        // getItem is called to instantiate the fragment for the given page.
        // Return a PlaceholderFragment (defined as a static inner class below).
        return ResourcesListFragment.newInstance(_resItems[position].resources)
    }

    override fun getPageTitle(position: Int): String? {
        return _resItems[position].type
    }

    override fun getCount(): Int {
        // Show 2 total pages.
        return _resItems.count()
    }
}