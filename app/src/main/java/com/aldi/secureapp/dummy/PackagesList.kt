package com.aldi.secureapp.dummy

import android.content.pm.PackageInfo

class PackagesList {
     var items: MutableList<PackageInfo> = mutableListOf()

    fun addItems(_items: MutableList<PackageInfo>) {
        items = _items
    }

    fun deleteItems() {
        items = mutableListOf()
    }

    fun getItem(name: String) : PackageInfo {
        return items.first { it.packageName == name }
    }
}