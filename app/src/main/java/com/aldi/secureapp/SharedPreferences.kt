package com.aldi.secureapp

import android.os.Bundle
import android.view.View
import android.widget.EditText
import androidx.appcompat.app.AppCompatActivity
import com.aldi.secureapp.Utilities.SharePreferenceSingelton
import java.lang.Exception

class SharedPreferences : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_shared_preferences)
    }

    fun saveAsSharePreference(_view: View) {
        val sharedPreferences = SharePreferenceSingelton.getSharedPreferences(applicationContext)
        val editView = findViewById<EditText>(R.id.sharedPreferencesText)
        with(sharedPreferences?.edit()) {
            try {
                this?.putString("My_preference_one", editView.text.toString())
                this?.commit()
                editView.setText("")
            } catch (e: Exception) {
            }
        }
    }

    fun getSharePreference(view: View) {
        val sharedPreferences = SharePreferenceSingelton.getSharedPreferences(applicationContext)
        val editView = findViewById<EditText>(R.id.sharedPreferencesText)
        val sharedString: String? = sharedPreferences?.getString("My_preference_one", "")
        editView.setText(sharedString)
    }

}
