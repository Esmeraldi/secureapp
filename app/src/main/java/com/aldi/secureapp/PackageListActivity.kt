package com.aldi.secureapp

import android.content.Intent
import android.content.pm.PackageInfo
import android.content.pm.PackageManager
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.RecyclerView
import com.google.android.material.snackbar.Snackbar
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView

import com.aldi.secureapp.dummy.PackagesList
import kotlinx.android.synthetic.main.activity_package_list.*
import kotlinx.android.synthetic.main.package_list_content.view.*
import kotlinx.android.synthetic.main.package_list.*

/**
 * An activity representing a list of Pings. This activity
 * has different presentations for handset and tablet-size devices. On
 * handsets, the activity presents a list of items, which when touched,
 * lead to a [PackageDetailActivity] representing
 * item details. On tablets, the activity presents the list of items and
 * item details side-by-side using two vertical panes.
 */
class PackageListActivity : AppCompatActivity() {
    companion object {
        val PACKAGE_ID = "package_id"
        val GET_PREFERENCES = "Get_preferences"
    }
    /**
     * Whether or not the activity is in two-pane mode, i.e. running on a tablet
     * device.
     */
    private var twoPane: Boolean = false

    private var packages: PackagesList = PackagesList();

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_package_list)

        setSupportActionBar(toolbar)
        toolbar.title = title

        if (package_detail_container != null) {
            // The detail container view will be present only in the
            // large-screen layouts (res/values-w900dp).
            // If this view is present, then the
            // activity should be in two-pane mode.
            twoPane = true
        }

        var packageItemsInstalled = packageManager.getInstalledPackages(PackageManager.MATCH_UNINSTALLED_PACKAGES).filter {
            !it.packageName.contains("samsung") &&  !it.packageName.contains("android")
        }

        packages.addItems(packageItemsInstalled as MutableList<PackageInfo>)
        setupRecyclerView(package_list)
    }

    private fun setupRecyclerView(recyclerView: RecyclerView) {
        recyclerView.adapter = SimpleItemRecyclerViewAdapter(this, packages.items, twoPane)
    }

    class SimpleItemRecyclerViewAdapter(private val parentActivity: PackageListActivity,
                                        private val values: MutableList<PackageInfo>,
                                        private val twoPane: Boolean) :
            RecyclerView.Adapter<SimpleItemRecyclerViewAdapter.ViewHolder>() {

        private val onClickListener: View.OnClickListener
        private  val onLongClickListener : View.OnLongClickListener

        init {
            onClickListener = View.OnClickListener {
                val _textView : TextView  = it.findViewById(R.id.package_item)
                val intent = Intent(parentActivity.baseContext, PackagesPage::class.java)
                intent.putExtra(PACKAGE_ID, _textView.text)
                parentActivity.startActivity(intent)
            }

            onLongClickListener = View.OnLongClickListener {
                val _textView : TextView  = it.findViewById(R.id.package_item)
                val intent = Intent(parentActivity.baseContext, PackagesPage::class.java)
                intent.putExtra(PACKAGE_ID, _textView.text)
                intent.putExtra(GET_PREFERENCES, true)
                parentActivity.startActivity(intent)
               return@OnLongClickListener  true
            }
        }

        override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
            val view = LayoutInflater.from(parent.context)
                    .inflate(R.layout.package_list_content, parent, false)
            return ViewHolder(view)
        }

        override fun onBindViewHolder(holder: ViewHolder, position: Int) {
            val item = values[position]
            holder.idView.text = item.packageName

            with(holder.itemView) {
                tag = item
                setOnClickListener(onClickListener)
                setOnLongClickListener(onLongClickListener)
            }
        }

        override fun getItemCount() : Int = values.size

        inner class ViewHolder(view: View) : RecyclerView.ViewHolder(view) {
            val idView: TextView = view.package_item
        }
    }
}
