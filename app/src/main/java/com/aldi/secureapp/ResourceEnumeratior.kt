package com.aldi.secureapp

import android.content.res.Resources
import android.util.Log

class ResourceEnumerator(
        private val resources: Resources?
) {

    /**
     * Wherein Dianne Hackborn explains resource ID generation in AAPT:
     * https://stackoverflow.com/questions/6517151/how-does-the-mapping-between-android-resources-and-resources-id-work
     */
    fun getResourceGroups(): List<ResourceGroup> {
        val groups = mutableListOf<ResourceGroup>()
        var nextStart = 0x7F010000
        while (true) {
            val group = getResources(nextStart)
            if (group != null) {
                groups.add(group)
                val lastId = group.resources.last().id
                nextStart = (lastId - (lastId % typeIterator)) + typeIterator
                Log.d("last resource: %s", group.resources.last().id.toHex())
                Log.d("next start: %s", nextStart.toHex())
            } else {
                break
            }
        }
        return groups
    }

    private fun getResources(startWithId: Int): ResourceGroup? {
        val entries = mutableListOf<ResourceEntry>()

        var i = startWithId
        while (true) {
            try {
                val name: String? = resources?.getResourceName(i)
                val type: String? = resources?.getResourceTypeName(i)
                val entry: String?  = resources?.getResourceEntryName(i)
                entries.add(ResourceEntry(i, name, type, entry))
            } catch (e: Resources.NotFoundException) {
                break
            }
            i++
        }

        return if (entries.isEmpty()) {
            null
        } else {
            ResourceGroup(
                    type = entries.first().type,
                    resources = entries
            )
        }
    }

    companion object {
        private const val typeIterator      = 0x00010000
    }

}

data class ResourceGroup(
        val type: String?,
        val resources: List<ResourceEntry>
)

data class ResourceEntry(
        val id: Int,
        val name: String?,
        val type: String?,
        val entry: String?
)

fun Int.toHex(): String = String.format("0x%08X", this)