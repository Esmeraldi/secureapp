package com.aldi.secureapp.Utilities


import android.content.Context
import android.content.SharedPreferences

object SharePreferenceSingelton {
    var sharedPreferences: SharedPreferences? =  null
    const val SHARED_PREFERENCES_SECURE_APP = "sharedpreferencessecureapp"

    fun getSharedPreferences(context: Context): SharedPreferences {
        return context.getSharedPreferences(SHARED_PREFERENCES_SECURE_APP,Context.MODE_WORLD_READABLE)
    }
}