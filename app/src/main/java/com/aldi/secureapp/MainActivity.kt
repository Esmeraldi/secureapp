package com.aldi.secureapp

import androidx.appcompat.app.AppCompatActivity

import android.content.Intent
import android.content.pm.PackageInfo
import android.content.pm.PackageManager
import android.os.Bundle
import android.view.View
import android.widget.TextView
import com.aldi.secureapp.Utilities.SharePreferenceSingelton

class MainActivity : AppCompatActivity() {


    private var _packageManager: PackageManager? = null
    private var installedApplications: List<PackageInfo>? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        _packageManager = getPackageManager()
    }

    fun getInstalledApplications(view: View) {
        installedApplications = _packageManager!!.getInstalledPackages(PackageManager.MATCH_UNINSTALLED_PACKAGES)
        val intent = Intent(baseContext, PackageListActivity::class.java)
        startActivity(intent)
    }


    fun goToSharedPreferences(view: View) {
        startActivity(Intent(baseContext, SharedPreferences::class.java))
    }

}
