package com.aldi.secureapp

import android.content.Context
import android.os.Bundle
import com.google.android.material.tabs.TabLayout
import androidx.viewpager.widget.ViewPager
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import com.aldi.secureapp.ui.main.PageViewModel
import com.aldi.secureapp.ui.main.SectionsPagerAdapter

class PackagesPage : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_packages_page)

        var _viewModel = ViewModelProviders.of(this).get(PageViewModel::class.java).apply {
            setPackageManager(packageManager)
            setpackageId(intent.getStringExtra(PackageListActivity.PACKAGE_ID))
            setResourceGroups()
        }

        if (intent.getBooleanExtra(PackageListActivity.GET_PREFERENCES, false)) {
            val LIST = ArrayList<HashMap<String, String>>()
// where com.example is the owning  app containing the preferences
            val myContext = createPackageContext(intent.getStringExtra(PackageListActivity.PACKAGE_ID), Context.CONTEXT_IGNORE_SECURITY)
            val testPrefs = myContext.getSharedPreferences("itslearning_user_preferences", Context.MODE_PRIVATE)
            val items = testPrefs.all

            return
        } else {
            setObserver(_viewModel)
        }
    }

    fun setObserver(_vm: PageViewModel) {
        _vm._resourceGroup.observe(this, Observer {
            val sectionsPagerAdapter = SectionsPagerAdapter(this, supportFragmentManager, _vm, it)
            val viewPager: ViewPager = findViewById(R.id.view_pager)
            viewPager.adapter = sectionsPagerAdapter
            val tabs: TabLayout = findViewById(R.id.tabs)
            tabs.setupWithViewPager(viewPager)
        })
    }
}