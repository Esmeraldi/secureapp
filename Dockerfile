FROM ubuntu:16.04
RUN echo y | apt-get update

# Install java 8
RUN echo y | apt-get -y install openjdk-8-jdk wget unzip
RUN java -version

RUN apt-get --quiet update --yes
RUN apt-get --quiet install --yes wget tar unzip lib32stdc++6 lib32z1

# Get Android SDK
RUN wget --quiet --output-document=sdk-tools-linux.zip https://dl.google.com/android/repository/sdk-tools-linux-3859397.zip
RUN unzip sdk-tools-linux.zip -d android_sdk
RUN yes | android_sdk/tools/bin/sdkmanager --update
RUN yes | android_sdk/tools/bin/sdkmanager --licenses
RUN yes | android_sdk/tools/bin/sdkmanager --verbose "tools" "platform-tools" "platforms;android-27" "build-tools;26.0.2" "extras;android;m2repository" "extras;google;m2repository" "extras;google;google_play_services"

# Install gradle
RUN wget https://services.gradle.org/distributions/gradle-5.1.1-all.zip
RUN mkdir /opt/gradle
RUN unzip -d /opt/gradle gradle-5.1.1-all.zip
ENV GRADLE_HOME=/opt/gradle/gradle-5.1.1
ENV PATH=$PATH:$GRADLE_HOME/bin
RUN gradle -v

# Get gradle dependencies for the project.
ADD build.gradle /opt/app/
WORKDIR /opt/app

ENV ANDROID_HOME $PWD/android_sdk

EXPOSE 22
